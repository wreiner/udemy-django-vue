
// Task list component
Vue.component('task-list', {
    props: {
        tasks: {
            type: Array,
            required: true
        }
    },
    data: function () {
        return {
            description: null,
            error: null
        }
    },
    methods: {
        addNewTask: function (event) {
            console.log("addNewTask has been called");

            if (this.description) {
                console.log("New Task: " + this.description);
    
                this.$emit('add-task', {
                    description: this.description
                })

                this.description = null;
                this.error = null;
            } else {
                this.error = "You must add a description"
            }
        },
        outerRemoveTask: function (task) {
            console.log("outer called");
            this.$emit('remove-task', task);
        }
    },
    template: `
        <div>
            <div v-if="error" class="alert alert-danger" role="alert">
                {{ error }}
            </div>

            <input class="mb-3 form-control" type="text" v-model="description" @keyup.enter="addNewTask">
            
            <task
                v-for="(task, idx) in tasks"
                :task="task"
                :key="idx"
                v-on:inner-remove-task="outerRemoveTask"
                ></task>
        </div>
    `
})

//   Task component
Vue.component('task', {
    props: {
        task: {
            type: Object,
            required: true
        }
    },
    methods: {
        innerRemoveTask: function (task) {
            console.log("remove been called");
            console.log(task);
            this.$emit('inner-remove-task', task);
        }
    },
    template: `
        <div class="alert alert-success alert-dismissible fade show" role="alert">
            {{ task.description }}
            <button type="button" class="close" data-dismiss="alert" aria-label="Close" @click.prevent="innerRemoveTask(task)">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
    `
})

// Main Vue instance
var app = new Vue({                                                                                                           
    el: '#app',
    data: {
        no_tasks_message: "To add a new task, write something and press enter!",
        tasks: []
    },
    computed: {
        taskCount: function () {
            return this.tasks.length;
        }

    },
    methods: {
        addTask: function(ntask) {
            console.log("function called..");
            console.log(ntask);
            this.tasks.push(ntask);
        },
        removeTask: function(rtask) {
            console.log("function called..");
            console.log(rtask);
            this.tasks.splice(this.tasks.indexOf(rtask), 1);
        }
    }
})
